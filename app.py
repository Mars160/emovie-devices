from config import DATABASE_STRING
from flask import Flask
from os import urandom
from blueprints import blueprints
from flask_jwt_extended import JWTManager
from ext import *

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = DATABASE_STRING
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SECRET_KEY'] = urandom(24)
app.config['JWT_SECRET_KEY'] = urandom(24)

db.init_app(app)

jwt = JWTManager(app)

for i in blueprints:
    app.register_blueprint(i)


@app.route('/')
def index():
    rules = app.url_map.__dict__['_rules']
    return ''.join(['<a href="%s"> %s </a><br>' % (i, i) for i in rules])


if __name__ == '__main__':
    app.run('0.0.0.0', 5000)

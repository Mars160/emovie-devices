from ext import db
from werkzeug.security import generate_password_hash, check_password_hash


class BorrowReasonType(db.Model):
    __tablename__ = 'BorrowReasonType'
    reasonType = db.Column(db.String(255), primary_key=True)
    available = db.Column(db.Boolean, default=True)
    level = db.Column(db.Integer, default=1)

    def __repr__(self):
        return '<Table %s>' % self.__tablename__


class DeviceBorrow(db.Model):
    __tablename__ = 'DeviceBorrow'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255))
    identity = db.Column(db.String(255))
    phone_number = db.Column(db.String(12))
    reason_type = db.Column(db.String(255))
    reason_name = db.Column(db.Text)
    device = db.Column(db.Text)
    return_device = db.Column(db.Text)
    state = db.Column(db.Enum(
        '未处理', '借出', '归还', '驳回', '待取', '部分还回'
    ))
    receive_time = db.Column(db.DateTime)
    actual_receive_time = db.Column(db.DateTime, nullable=True)
    return_time = db.Column(db.DateTime)
    actual_return_time = db.Column(db.DateTime, nullable=True)
    note = db.Column(db.Text, nullable=True)
    lenter = db.Column(db.VARCHAR(255), nullable=True)

    def __repr__(self):
        return '<Table %s>' % self.__tablename__


class DeviceInfo(db.Model):
    __tablename__ = 'DeviceInfo'
    name = db.Column(db.String(255), primary_key=True)
    category = db.Column(db.VARCHAR(255), nullable=False)
    permission = db.Column(db.Integer, nullable=False)
    tag = db.Column(db.Text, nullable=True)
    family = db.Column(db.Text, nullable=True)

    def __repr__(self):
        return '<Table %s>' % self.__tablename__


class DeviceStatus(db.Model):
    __tablename__ = 'DeviceStatus'
    name = db.Column(db.String(255), primary_key=True)
    id = db.Column(db.Integer, primary_key=True)
    state = db.Column(db.Enum(
        '在库', '借出', '遗失', '预定'
    ), default='在库')
    place = db.Column(db.VARCHAR(255), nullable=False)

    def __repr__(self):
        return '<Table %s>' % self.__tablename__


class DeviceTypes(db.Model):
    __tablename__ = 'DeviceTypes'
    type = db.Column(db.VARCHAR(255), primary_key=True)
    available = db.Column(db.BOOLEAN, default=True)

    def __repr__(self):
        return '<Table %s>' % self.__tablename__


class HandOver(db.Model):
    __tablename__ = 'HandOver'
    lastWeek = db.Column(db.String(255))
    thisWeek = db.Column(db.String(255))
    havingDevice = db.Column(db.JSON, nullable=True)
    time = db.Column(db.TIMESTAMP, primary_key=True)

    def __repr__(self):
        return '<Table %s>' % self.__tablename__


class Member(db.Model):
    __tablename__ = 'Member'
    name = db.Column(db.String(255))
    grade = db.Column(db.Integer)
    id = db.Column(db.BIGINT, primary_key=True)
    profession = db.Column(db.VARCHAR(255), nullable=False)
    birthday = db.Column(db.Date)
    phone = db.Column(db.String(20))
    email = db.Column(db.String(50))
    password = db.Column(db.VARCHAR(255), name='_Password')
    level = db.Column(db.Integer)

    def __repr__(self):
        return '<Table %s>' % self.__tablename__

    def setPassword(self, password=None):
        if password is None:
            password = "emovieECNU@" + str(self.id)
        self.password = generate_password_hash(password)

    def checkPassword(self, password):
        return check_password_hash(self.password, password)


class Places(db.Model):
    __tablename__ = 'Places'
    place = db.Column(db.VARCHAR(255), primary_key=True)
    available = db.Column(db.BOOLEAN, default=True)
    
    def __repr__(self):
        return '<Table %s>' % self.__tablename__


class Profession(db.Model):
    __tablename__ = 'Profession'
    profession = db.Column(db.VARCHAR(255), primary_key=True)
    available = db.Column(db.BOOLEAN, default=True)

    def __repr__(self):
        return '<Table %s>' % self.__tablename__


class UserLevel(db.Model):
    __tablename__ = 'UserLevel'
    level = db.Column(db.Integer, primary_key=True)
    nickname = db.Column(db.VARCHAR(255), nullable=False)

    def __repr__(self):
        return '<Table %s>' % self.__tablename__


class Devices(db.Model):
    __tablename__ = 'Devices'
    name = db.Column(db.String(255), primary_key=True)
    category = db.Column(db.VARCHAR(255), nullable=False)
    family = db.Column(db.Text, nullable=True)
    permission = db.Column(db.Integer, nullable=False)
    tag = db.Column(db.Text, nullable=True)
    id = db.Column(db.Integer, nullable=False, primary_key=True)
    state = db.Column(db.Enum(
        '在库', '借出', '遗失', '预定'
    ), default='在库')
    place = db.Column(db.VARCHAR(255), nullable=False)

    def __repr__(self):
        return '<Table %s>' % self.__tablename__












// vue.config.js
const AutoImport = require('unplugin-auto-import/webpack')
const Components = require('unplugin-vue-components/webpack')
const { ElementPlusResolver } = require('unplugin-vue-components/resolvers')

module.exports = {
    productionSourceMap: false,
    publicPath: './', // 公共路径
    // webpack-dev-server 相关配置  
    devServer: {
        port: 8989,
        proxy: "http://localhost:5000"
    },
    //其他配置....
    configureWebpack: {
        plugins: [
            AutoImport({
                resolvers: [ElementPlusResolver()],
            }),
            Components({
                resolvers: [ElementPlusResolver()],
            }),
        ],
    }
}
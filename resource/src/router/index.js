import { createRouter, createWebHashHistory } from 'vue-router'
import InspectMain from "@/components/Inspect_Main";
import DeviceCheckMain from "@/components/DeviceCheck_Main";
import Borrow_Main from "@/components/Borrow_Main";
import Approve_Main from "@/components/Approve_Main";
import Result_Main from "@/components/Result_Main";
import Login from "@/components/Login";
import Guide from "@/components/Guide";
import Empty from "@/components/Empty";
import MovePlace from "@/components/MovePlace"
import ChangePwd from "@/components/ChangePWD"
import User_Main from "@/components/User_Main";

const routes = [
  {
    path: '/inspect',
    component: InspectMain,
  },
  {
    path: '/device-check',
    component: DeviceCheckMain
  },
  {
    path: '/borrow',
    component: Borrow_Main
  },
  {
    path: '/approve',
    component: Approve_Main
  },
  {
    path: '/result/:msg',
    component: Result_Main,
  },
  {
    path: '/login',
    component: Login,
  },
  {
    path: '/guide',
    component: Guide,
  },
  {
    path: '/',
    component: Empty,
  },
  {
    path: '/movePlace',
    component: MovePlace
  },
  {
    path: '/changePwd',
    component: ChangePwd,
  },
  {
    path: '/user-manage',
    component: User_Main,
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

const Logined = () => {
  return localStorage.token;
}

router.beforeEach((to, from, next) => {
  switch (to.path) {
    case '/borrow':
    case '/login':
    case '/guide':
      next()
      break
    default:
      if(Logined()) {
        next()
      }else {
        next({path: 'login'})
      }
  }
})

export default router

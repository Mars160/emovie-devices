import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import axios from "axios";

axios.interceptors.request.use(
    (config) => {
        const token = localStorage['token'];
        if (token) {
            config.headers.Authorization = "Bearer " + token
        }
        return config
    }
)

const app = createApp(App)
app.config.globalProperties.$username = null
app.config.globalProperties.$stuID = null

//0代表无权限，1代表需成员权限，2代表需管理员权限，3代表需超级管理员权限
app.config.globalProperties.$afterLoginPages = {
    'borrow': ['借用申请', 0],
    'approve': ['借用审批', 1],
    'device-check': ['设备交接', 1],
    'inspect': ['设备审查', 1],
    'changePwd': ['密码修改', 1],
    'user-manage': ['用户管理', 2],
    //'device-manage': ['设备管理', 2],
}
app.use(router).mount('#app')
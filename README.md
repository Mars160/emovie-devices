壹幕工作室设备借还清点系统

设备借还流程：

用户进入网页->填写名字学号等基本信息->根据名字和学号提供可借用在库设备->用户选择自己想借用的设备->用户提交->发邮件提醒值日生->值日生进入审批页面->编辑用户选择的设备->批准或驳回

已完成：

设备清点（前端+后端）

设备核验（前端+后端）

设备借用（前端+后端）

借用记录查询编辑（前端+后端）

待完成：

设备表分离（计算机楼和大演播室）

设备借用邮箱提醒

成员生日邮箱提醒



记录检查/编辑	https://emovie-device.somewang.top/#/approve

设备借用	https://emovie-device.somewang.top/#/borrow

设备情况检查	https://emovie-device.somewang.top/#/inspect

设备清点	https://emovie-device.somewang.top/#/device-check

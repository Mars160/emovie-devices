from flask import Blueprint
from models import *

device = Blueprint('device', __name__)


@device.route('/api/deviceType', methods=['GET'])
def getDeviceType():
    return {'type': [i.type for i in DeviceTypes.query.filter(DeviceTypes.available == 1).all()]}


@device.route('/api/devicePlace', methods=['GET'])
def getDevicePlace():
    return {'place': [i.place for i in Places.query.filter(Places.available == 1).all()]}

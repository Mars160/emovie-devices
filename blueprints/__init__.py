from .duty import duty
from .return_data import return_data
from .borrow import borrow
from .record import record_bp
from .user import _user
from .devices import device


blueprints = [duty, return_data, borrow, record_bp, _user, device]

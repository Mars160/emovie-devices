from smtplib import SMTP_SSL
from email.mime.text import MIMEText


def sendMail(Subject, message, to_addrs, sender_show='壹幕工作室', recipient_show='收件人', cc_show=''):
    '''
    :param Subject: str 邮件主题描述
    :param message: str 邮件内容
    :param to_addrs: str 实际收件人
    :param sender_show: str 发件人显示，不起实际作用如："xxx"
    :param recipient_show: str 收件人显示，不起实际作用 多个收件人用','隔开如："xxx,xxxx"
    :param cc_show: str 抄送人显示，不起实际作用，多个抄送人用','隔开如："xxx,xxxx"
    '''
    # 填写真实的发邮件服务器用户名、密码
    user = '1013059780@qq.com'
    password = 'aqulpiehgvalbeba'
    # 邮件内容
    msg = MIMEText(message, 'plain', _charset="utf-8")
    # 邮件主题描述
    msg["Subject"] = Subject
    # 发件人显示，不起实际作用
    msg["from"] = sender_show
    # 收件人显示，不起实际作用
    msg["to"] = recipient_show
    # 抄送人显示，不起实际作用
    msg["Cc"] = cc_show
    with SMTP_SSL(host="smtp.qq.com",port=465) as smtp:
        # 登录发邮件服务器
        smtp.login(user = user, password = password)
        # 实际发送、接收邮件配置
        smtp.sendmail(from_addr = user, to_addrs=to_addrs.split(','), msg=msg.as_string())

if __name__ == '__main__':
    message = 'Python 测试邮件...'
    Subject = '主题测试'
    to_addrs = '1013059780@qq.com'
    sendMail(message,Subject,to_addrs)
    print('Finish')
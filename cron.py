from mail import sendMail
from models import Member
from config import DATABASE_STRING
from sqlalchemy import create_engine, and_, extract
from sqlalchemy.orm import sessionmaker
from datetime import date, timedelta

engine = create_engine(DATABASE_STRING)
session = sessionmaker(bind=engine)()

today = date.today()
tomorrow = date.today() + timedelta(days=1)

member = session.query(Member).filter(
    and_(extract('month', Member.birthday) == today.month, extract('day', Member.birthday) == today.day)
).all()

tom_member = session.query(Member).filter(
    and_(extract('month', Member.birthday) == tomorrow.month, extract('day', Member.birthday) == tomorrow.day)
).all()

emails = session.query(Member).filter(
    Member.Type != '成员'
).all()

if len(member) != 0 or len(tom_member) != 0:
    names = []
    for i in member:
        names.append(i.name)

    content = "" if len(names) == 0 else "今天是%s的生日\r\n" % ",".join(names)

    names = []
    for i in tom_member:
        names.append(i.name)

    content += "" if len(names) == 0 else "明天是%s的生日\r\n" % ",".join(names)
    content += "此邮件为壹幕工作室程序自动发送，请勿回复~\r\n"

    for i in emails:
        sendMail("成员生日信息", content, i.email)
    print(today, content)
else:
    print(today, tomorrow, "no body")
